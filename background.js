var repo = $('body').attr('data-current-repo');

if (repo !== undefined) {
    repo = JSON.parse(repo);

    var clonePath = "github-windows://openRepo/https://bitbucket.org/" + repo.creator.username + "/" + repo.slug;

    jQuery('#repo-header .aui-page-header-inner').css('position', 'relative');
    jQuery('#repo-toolbar').css('vertical-align', 'top');

    jQuery('<a/>', {
        href: clonePath,
        title: 'Clone in GitHub for Windows',
        text: 'Clone in GitHub for Windows',
        // class: 'aui-button aui-button-primary',
        style: "float: right; position: absolute; right: 0; bottom: 0;"
    }).appendTo('#repo-header .aui-page-header-inner');
}